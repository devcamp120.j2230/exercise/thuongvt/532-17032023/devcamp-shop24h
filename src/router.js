import HomePage from "./pages/homePage"
import ProductList from "./pages/ProductList"

const RouteList = [
    {label:"homePage", path:"/", element:<HomePage></HomePage>},
    {label:"Product list", path:"/products", element:<ProductList></ProductList>}
]
export default RouteList