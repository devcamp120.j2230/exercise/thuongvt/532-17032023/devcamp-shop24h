import { Grid, Card, CardActionArea, CardContent, CardMedia, Typography, Pagination} from "@mui/material"
import { Container } from "@mui/system";
import { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
function ProductCart() {

    const [product, setProduct] = useState([]);
    const [totalPage, setTotalPage] = useState(0)
    const [curPage, setCurPage] = useState(1)


    const fetchAPI = async (url, requestOptions) => {
        let res = await fetch(url, requestOptions);
        let data = await res.json()
        return data
    }
    const getAllHandler = () => {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };
        var url = "http://localhost:8000/products"
        fetchAPI(url, requestOptions)
            .then((response) => {
                setTotalPage(Math.ceil(response.Data.length/3));
                setProduct(response.Data);
                console.log(curPage)
            })
            .catch((err) => {
                console.error(err)
            })
    }
    const handleChangePage = (e,v)=>{
        setCurPage(v)
    }

    useEffect(() => {
        getAllHandler()
    }, [curPage]);

    return (<>
    <Grid item xs={12} sm={12} md={12} style={{ display: "flex", flexWrap: "wrap", justifyContent: "center" }}>
                {product.slice(curPage, curPage+9).map((element, index) => {
                    return <Card key={index} sx={{ maxWidth: 250 }} style={{ marginRight: "30px", marginTop: "15px", marginBottom: "15px" }}>
                        <CardActionArea>
                            <CardMedia component="img"
                                image={require("../../assetment/image/" + element.mageUrl)}
                                height="250px"
                                width="250px"
                                style={{ textAlign: "center" }}>
                            </CardMedia>
                        </CardActionArea>
                        <CardContent style={{ textAlign: "center" }}>
                            <Typography style={{ marginTop: "10px", fontSize: "20px" }}>
                                {element.Name}
                            </Typography>
                            <Typography >
                                <del>{element.PromotionPrice}</del>{" "}<span className="text-danger fw-bold" style={{ fontSize: "20px" }}>{element.BuyPrice}</span>
                            </Typography>
                            <Typography component={Button} className={"btn-success"}>Chi tiết</Typography>
                        </CardContent>
                    </Card>
                })}
            </Grid>
             <Container>
             <Grid item style={{display:"flex", justifyContent:"end", marginBottom:"10px"}} >
                     <Pagination count={totalPage} defaultPage={curPage} onChange={handleChangePage}></Pagination>
             </Grid>
         </Container>
    </>
    )
}

export default ProductCart