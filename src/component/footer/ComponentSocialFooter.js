import { Grid, Typography } from "@mui/material"
import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';
import YouTubeIcon from '@mui/icons-material/YouTube';
import TwitterIcon from '@mui/icons-material/Twitter';


function ComponentSocialFooter(){
    return(
        <>
        <Grid item lg={3} style={{ paddingLeft: "100px" }}>
            <Typography style={{ fontSize: '30px', fontWeight: "bold" }}>
            DevCamp
            </Typography>
            <Grid>
                <FacebookIcon style={{paddingRight: "10px"}} fontSize="large"></FacebookIcon>
                <InstagramIcon style={{paddingRight: "10px"}} fontSize="large"></InstagramIcon>
                <YouTubeIcon style={{paddingRight: "10px"}} fontSize="large"></YouTubeIcon>
                <TwitterIcon style={{paddingRight: "10px"}} fontSize="large"></TwitterIcon>
            </Grid>
        </Grid>
        </>
    )
}
export default ComponentSocialFooter